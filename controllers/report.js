var mongoose = require('mongoose');
var Room = mongoose.model('Room');

/**
 * Return all on/off switching for a month
 * @param req
 * @param res
 */
exports.switchActions = (req, res) => {
    Room.getAllActions(req.params.m, req.params.y, (err, data) => {
        if (err)
            res.status(500).end();
        else
            res.json(data);
    });

    /**
     * Return time percent for each day when lights where on/off
     * @param req
     * @param res
     */
    exports.switchUsage = (req, res) => {
        Room.getUsage(req.params.m, req.params.y, (err, data) => {
            if (err)
                res.status(500).end();
            else
                res.json(data);
        });
    };

    //Room.findByName('Room1', function (err, room) {

};

/**
 * Return all on/off switching for a month
 * @param req
 * @param res
 */
exports.getData = (req, res, next) => {
    Room.getData((err, data) => {
        if (err)
            next(err);
        else
            res.json(data);
    });
}