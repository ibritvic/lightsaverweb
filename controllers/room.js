var mongoose = require('mongoose');
var Room = mongoose.model('Room');

exports.create = function (req, res, next) {
    // if (!req.xhr) No need for Ajax
    var room = new Room(req.body);
    if (!room.name) {
        res.status(400).send('Room name cannot be blank');
        return;
    }
    room.save(function (err) {
        if (err) next(err)
        res.status(200).send('Created!');
    });
};

/**
 * List of all rooms
 */

exports.list = function (req, res){
    var page = (req.params.page > 0 ? req.params.page : 1) - 1;
    var perPage = 30;
    var options = {
        perPage: perPage,
        page: page
    };

    Room.list(options, function (err, rooms) {
        if (err)
            return next(err);
        else
            res.json(rooms);
    });
};
/**
 * Find a room with id
 */

exports.load = function (req, res, next, id){
    Room.load(id, function (err, room) {
        if (err) return next(err);
        if (!room) return next(new Error('not found'));
        req.room = room;
        next();
    });
};

exports.listSwitches = function (req, res, next){
    res.json(req.room.switches);
};

exports.addSwitch = function (req, res, next){
    req.room.addSwitch(req.body, function (err, room) {
        if (err) return next(err);
        if (!room) return next(new Error('not found'));
        res.status(200).end();
    });
};

exports.removeSwitch = function (req, res, next){
    req.room.removeSwitch(req.params.deviceId, function (err, room) {
        if (err) return next(err);
        if (!room) return next(new Error('not found'));
    });
};

exports.listSensors = function (req, res, next){
    res.json(req.room.sensors);
};

exports.addSensor = function (req, res, next){
    req.room.addSensor(req.body, function (err, room) {
        if (err) return next(err);
        if (!room) return next(new Error('not found'));
        res.status(200).end();
    });
};

exports.removeSensor = function (req, res, next){
    req.room.removeSensor(req.params.deviceId, function (err, room) {
        if (err) return next(err);
        if (!room) return next(new Error('not found'));
    });
};
