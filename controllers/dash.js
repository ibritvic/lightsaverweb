"use strict";
var mongoose = require('mongoose');
var Room = mongoose.model('Room');

/**
 * Return aggregated data for rooms
 * @param req
 * @param res
 */
exports.data = (req, res) => {
        res.json(fakeData());
};

function fakeData(){
    let tempData = [21.5, 22, 22.2, 25, 26, 27, 24, 26, 25, 24, 22, 21.9, 21.7, 22.8, 23, 23.1, 23.1, 23, 23, 23.1, 23.2, 23.4, 23.3, 23.2];
    let lightData = [0, 0, 0, 0, 26, 150, 210, 540, 570, 578, 580, 780, 770, 850, 910, 810, 640, 630, 620, 440, 423, 12, 0, 0] ;
    let occupancyData = [0,0,0,0,0,0,0,1,1,1,1,1,1,1,1,1,1,1,1,1,0,0,0,0];
    let data = [{ temperature: tempData, light: lightData , occupancy : occupancyData}];
    return [{name:'VSITE Hall', data}, {name:'Test location', data} ];
}
