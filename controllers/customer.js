var mongoose = require('mongoose');
var Customer = mongoose.model('Customer');

exports.create = function (req, res, next) {
    // if (!req.xhr) No need for Ajax
    var customer = new Customer(req.body);
    if (!customer.name) {
        res.status(400).send('Customer name cannot be blank');
        return;
    }
    customer.save(function (err) {
        if (err) next(err)
        res.send('Created!');
    });
};

/**
 * List
 */

exports.list = function (req, res){
    var page = (req.params.page > 0 ? req.params.page : 1) - 1;
    var perPage = 30;
    var options = {
        perPage: perPage,
        page: page
    };

    Customer.list(options, function (err, customers) {
        if (err)
            return res.status(500).end();
        else
            res.json(customers);
    });
};

