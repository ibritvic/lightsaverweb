/**
 * Created by ibritvic
 */

var mongoose = require("mongoose");

var CustomerSchema = new mongoose.Schema({
    name: String,
    country: String,
    tel: String,
    email: String
});

/**
 * Validations
 */

CustomerSchema.path('email').required(true, 'Email cannot be blank');

/**
 * Statics
 */

CustomerSchema.statics = {

    /**
     * Find customer by id
     *
     * @param {ObjectId} id
     * @param {Function} cb
     * @api private
     */

    load: function (id, cb) {
        this.findOne({ _id : id })
            .exec(cb);
    },

    /**
     * List customers
     *
     * @param {Object} options
     * @param {Function} cb
     * @api private
     */

    list: function (options, cb) {
        var criteria = options.criteria || {}

        this.find(criteria)
            .sort({'createdAt': -1}) // sort by date
            .limit(options.perPage)
            .skip(options.perPage * options.page)
            .exec(cb);
    }
}

//Register model
mongoose.model('Customer', CustomerSchema);