/**
 * Created by ibritvic
 */
"use strict";

var mongoose = require("mongoose");
var Schema = mongoose.Schema;
var utils = require('../lib/utils');

var RoomSchema = new Schema({
    name: String,
    customer: {type : Schema.ObjectId, ref : 'Customer'},
    switches: [{
        id: Number, //from local board db (lokijs)
        name: String,
        actions: [] /*{ Don't do subschema for now
            action: Boolean,
            time: {type: Date, default: Date.now}
        }]*/
    }],
    sensors: [{
        id: Number, //from local board db (lokijs)
        name: String,
        values: [] /*{ Don't do subschema for now
            value: Number,
            type: String,
            time: {type: Date, default: Date.now}
        }]*/
    }],
    "location": {
        "type": {type: String, default: "Point"},
        "coordinates": {type: Array, default: [0,0]}
    },
    tel: String
});

/**
 * Validations
 */

RoomSchema.path('name').required(true, 'Name cannot be blank');

// RoomSchema.index({ 'location': '2dsphere' }); leade for later

RoomSchema.methods = {

    /**
     * Add switch
     *
     * @param {Object} switch
     * @param {Function} cb
     */

    addSwitch: ( sw, cb) => {

        this.switches.push(sw);
        this.save(cb);
    },

    /**
     * Remove sensor
     *
     * @param {deviceId} String
     * @param {Function} cb
     */

    removeSwitch:  function(deviceId, cb){
        var index = utils.indexof(this.switches, { id: deviceId });
        if (~index) this.switches.splice(index, 1);
        else return cb('not found');
        this.save(cb);
    },

    /**
     * Add sensor
     *
     * @param {Object} sensor
     * @param {Function} cb
     */

    addSensor:  ( sw, cb) => {

        this.sensors.push(sw);
        this.save(cb);
    },

    /**
     * Remove sensor
     *
     * @param {deviceId} String
     * @param {Function} cb
     */

    removeSensor: function(deviceId, cb){
        var index = utils.indexof(this.sensors, { id: deviceId });
        if (~index) this.sensors.splice(index, 1);
        else return cb('not found');
        this.save(cb);
    }

};

/**
 * Statics
 */

RoomSchema.statics = {

    /**
     * Upload data rooms
     *
     * @param {Object} room data
     * @param {Function} cb
     * @api private
     **/
//TODO: First time creates room and dooesn't save values and switches
    uploadData : function(uploadRooms){ ///WTF??? Arrow function not working here, this is empty object
        var rooms = []; //Database fetched rooms
        for (let r of uploadRooms ){
            let roomName = r.name || 'Room1';
            //find room if doesn't exist create it
            rooms.push( this.findOne({name: roomName}) );
        }
        return new Promise ((resolve, reject) => {
            Promise.all(rooms)
                .then((values) => {
                    var savePromises = [];
                    for (let i = 0; i < values.length; i++) {
                        let uploadRoom = uploadRooms[i];
                        let room = values[i] || new Room({name: uploadRoom.name, switches: [], sensors: []});
                        for (let uploadSensor of uploadRoom.sensors) {
                            if (!uploadSensor.values || !uploadSensor.values.length) continue;
                            var sensor = room.sensors.filter(s => s.id == uploadSensor.id)[0]; //TODO: find instead of filter
                            if (!sensor) {
                                sensor = {id: uploadSensor.id, name: uploadSensor.name, values: []};
                                room.sensors.push(sensor);
                            }
                            //concat doesn't work on subscheme, now is OK
                            for (let value of uploadSensor.values)
                                sensor.values.push(value);
                        }

                        for (let uploadSwitch of uploadRoom.switches) {
                            if (!uploadSwitch.actions || !uploadSwitch.actions.length) continue;
                            var sw = room.switches.filter(s => s.id == uploadSwitch.id)[0];
                            if (!sw) {
                                sw = {id: uploadSwitch.id, name: uploadSwitch.name, actions: []};
                                room.switches.push(sw);
                            }

                            for (let action of uploadSwitch.actions)
                                sw.actions.push(action);
                        }

                        savePromises.push(room.save());
                    }
                    Promise.all(savePromises)
                        .then(()=> resolve()); //Finally over, saves completed
                })
                .catch((err)=> {
                    reject(err);
                });
        })

    },

    /**
     * GetAllData as json
     */
    getData: function(cb){
        this.find({}, function(err, rooms){
            if(err) return cb(err); //old style
            var data = [];
            rooms.forEach(function(room) {
                data.push({ name: room.name, sensors: room.sensors, switches: room.switches});
            });

            cb(null, data);
        });


    },

    /**
     * Find room by id
     *
     * @param {ObjectId} id
     * @param {Function} cb
     * @api private
     */

    load: function (id, cb) {
        this.findOne({ _id : id })
            .populate('customer', 'name')
            .exec(cb);
    },

    /**
     * List rooms
     *
     * @param {Object} options
     * @param {Function} cb
     * @api private
     */

    list: function (options, cb) {
        console.log(JSON.stringify(this))
        var criteria = options.criteria || {};

        this.find(criteria)
            .populate('customer', 'name')
            .sort({'createdAt': -1}) // sort by date
            .limit(options.perPage)
            .skip(options.perPage * options.page)
            .exec(cb);
    },
    /**
     * Nr of all actions in all of the switches
     * @param month
     * @param year
     * @param cb
     */
    getAllActions: function(month, year, cb){
        var switches = this.find({ switches: { $exists: true }}, {'switches': 1});

    }
};

//Register model
var Room = mongoose.model('Room', RoomSchema);