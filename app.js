var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
//var nunjucks = require('nunjucks');

var fs = require('fs');

var app = express();
/* nunjucks out for now , not working with virtualhost, back to swig
// view engine setup
nunjucks.configure('views', {
    autoescape: true,
    express: app
});
*/
var swig = require('swig');
// Swig templating engine settings, maybe change for prod
swig.setDefaults({
    cache: false
});

// set views path, template engine and default layout
app.engine('html', swig.renderFile);


app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'html');
app.use(favicon(__dirname + '/public/images/favicon.ico'));
app.use(logger('dev'));
app.use(bodyParser.json({limit: '5mb'}));
app.use(bodyParser.urlencoded({limit: '5mb'}));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

// Bootstrap models
fs.readdirSync(__dirname + '/models').forEach(function (file) {
    if (~file.indexOf('.js')) require(__dirname + '/models/' + file);
});

//Now on with routes

var routes = require('./routes');
var user = require('./routes/user');
var report = require('./routes/report');

// redirect to logit - ugly
app.use(function(req,resp,next){
    if(!req.secure && req.headers.host.indexOf('logit') != -1){
        resp.redirect("https://" + req.headers.host + req.url);
    } else {
        return next();
    }
});


app.use('/', routes);
app.use('/user', user);
app.use('/report', report);

/// catch 404 and forwarding to error handler
app.use(function(req, res, next) {
    res.status(404);
    res.render('404');
});

/// error handlers

// development error handler
// will print stacktrace
//if (app.get('env') === 'development' || app.get('env') === 'test') {
if ( app.get('env') === 'test') { //samo za test
    app.use(function(err, req, res, next) {
        res.status(err.status || 500); //.send(err.message);
       res.render('error', {
            message: err.message,
            error: err
        });

    });
}

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.render('500');
});


module.exports = app;
