module.exports = {
    development: {
        db: 'mongodb://localhost/light',
        port: 3009,
        mqtt_port: 1883
    },
    test: {
        db: 'mongodb://localhost/mocha',
        port: 3009
    },
    production: {
        db: 'mongodb://localhost/light',
        port: 80
    }
};
