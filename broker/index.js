"use strict";
/**
 * MQTT Broker
 */

var mosca = require('mosca');
var config = require("../config")[process.env.NODE_ENV ||'development'];
var nrClients = 0;


exports.start = () => {
    //Start a new mqtt broker server, no persistance
    var server = new mosca.Server({port:config.mqtt_port});

    server.on('clientConnected', (client) => {
        console.log('client connected', client.id);
        nrClients++;
    });

// fired when a client disconnects
    server.on('clientDisconnected', (client)=> {
        console.log('Client Disconnected:', client.id);
        nrClients--;
    });

// fired when a message is received
    server.on('published', (packet, client) => {

        console.log('Published packet:', packet);
        console.log('Client:', client);
    });

    server.on('ready', () => {
        console.log('Mosca MQTT broker running on port: ' + config.mqtt_port);
    });
};
