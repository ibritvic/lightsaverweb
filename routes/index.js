/**
 * Created by ibritvic on 10/18/16.
 */
"use strict";
var express = require('express');
var router = express.Router();
var customer = require('../controllers/customer');
var room = require('../controllers/room');
var board = require('../controllers/board');
var dash = require('../controllers/dash');

/* Basic routes */

router
    .get('/', function(req, res, next) {
        if ( req.headers.host.indexOf('dashboard') !== -1 ) {
            res.render('dashboard', { title: 'Eter.io Dashboard' });
        } else {
            res.render('index', { title: 'Eter io' });
        }
    })

    .get('/hr', (req, res, next) =>{
            res.render('index_hr', { title: 'Eter io' });
    })


    //Upload data from board
    .post('/board/data', board.savePost)

    //Customer
    .get('/customer/all', customer.list)
    .post('/customer/new', customer.create)

    //Room
    .get('/room/all', room.list)
    .post('/room/new', room.create)

    .param('id', room.load) //Is it smart to use param?

    //Switches & Sensors
    .get('/room/:id/switch/list', room.listSwitches)
    .post('/room/:id/switch/new', room.addSwitch)
    .delete('/room/:id/switch/delete/:deviceId', room.removeSwitch)

    .get('/room/:id/sensor/list', room.listSensors)
    .post('/room/:id/sensor/new', room.addSensor)
    .delete('/room/:id/sensor/delete/:deviceId', room.removeSensor)


    //Report data
    .get('/report/sensors')


router.get('/dashboard', function(req, res, next) {
    res.render('dashboard', { title: 'Eter.io Dashboard' });
});

router.get('/dash/data', dash.data);

//Data from board
router.post('/board/data', board.savePost);

//Customer
router.get('/customer/all', customer.list);
router.post('/customer/new', customer.create);

//Room
router.get('/room/all', room.list);
router.post('/room/new', room.create);


router.param('id', room.load); //Is it smart to use param?

//Switches & Sensors
router.get('/room/:id/switch/list', room.listSwitches);
router.post('/room/:id/switch/new', room.addSwitch);
router.delete('/room/:id/switch/delete/:deviceId', room.removeSwitch);

router.get('/room/:id/sensor/list', room.listSensors);
router.post('/room/:id/sensor/new', room.addSensor);
router.delete('/room/:id/sensor/delete/:deviceId', room.removeSensor);


//Report data
router.get('/report/sensors')



module.exports = router;

