var express = require('express');
var router = express.Router();
var report = require('../controllers/report');

/* GET users listing. */
router.get('/switch/actions', report.switchActions);

/* GET users listing. */
router.get('/data', report.getData);

module.exports = router;
