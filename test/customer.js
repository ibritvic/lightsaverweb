/**
 * Created by ibritvic on 06/10/15.
 */
var mongoose = require('mongoose')
    , should = require('should')
    , request = require('supertest');



var count;


describe('Customers', function (done) {
    //Start
    var env = process.env.NODE_ENV;
    var agent, Customer;
    before(function () {
        process.env.NODE_ENV = "test";
        //   delete require.cache[require.resolve('../bin/server')]; //clear cache
        server = require('../bin/server');
        agent = request(server);
        Customer = mongoose.model('Customer');
    });


    after(function (done) {
        Customer.collection.remove();
        process.env.NODE_ENV = env;
        done()
        // server.close(done); ??? server closed here
    });


    describe('GET /', function () {
        it('should respond with Content-Type text/html', function (done) {
            agent
                .get('/')
                .expect('Content-Type', /html/)
                .expect(200, done)
        })
    });

    describe('POST /customer', function () {
        before(function (done) {
            Customer.count(function (err, cnt) {
                count = cnt
                done()
            })
        })

        describe('Invalid parameters', function () {
            it('should respond with error', function (done) {
                agent
                    .post('/customer/new')
                    .send({'name': ''})
                    .expect('Content-Type', /html/)
                    .expect(400)
                    .expect(/Customer name cannot be blank/)
                    .end(done)
            })
            it('email blank, should respond with error', function (done) {
                agent
                    .post('/customer/new')
                    .send({'name': 'dsadd'})
                    //   .expect(/Email/) not working, Mongoose validatiom message not good, who cares
                    .expect(500)
                    .end(done)
            })

            it('should not save to the database', function (done) {
                Customer.count(function (err, cnt) {
                    count.should.equal(cnt)
                    done()
                })
            })
        })

        describe('Valid parameters', function () {
            it('should send customer created', function (done) {
                agent
                    .post('/customer/new')
                    .send({'name': 'foo', 'email':'a@a.hr'})
                    .expect('Content-Type', /html/)
                    .expect(200)
                    .end(done)
            })

            it('should insert a record to the database', function (done) {
                Customer.count(function (err, cnt) {
                    cnt.should.equal(count + 1)
                    done()
                })
            })

            it('should save the customer to the database', function (done) {
                Customer
                    .findOne({name: 'foo'})
                    .exec(function (err, customer) {
                        should.not.exist(err);
                        customer.should.be.an.instanceOf(Customer);
                        customer.name.should.equal('foo');
                        done()
                    })
            })
        })
    })


});