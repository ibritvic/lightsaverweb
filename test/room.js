/**
 * Created by ibritvic on 06/10/15.
 */
var mongoose = require('mongoose')
    , should = require('should')
    , request = require('supertest')
    , async = require('async');


describe('Rooms', function (done) {
    //Start
    var env = process.env.NODE_ENV;
    var count, agent, Room, id=0;

    before(function () {
        process.env.NODE_ENV = "test";
        //   delete require.cache[require.resolve('../bin/server')]; //clear cache
        server = require('../bin/server');
        agent = request(server);
        Room = mongoose.model('Room');
    });


    after(function (done) {
        Room.collection.remove();
        process.env.NODE_ENV = env;
        done()
        // server.close(done); ??? server closed here
    });


    describe('POST /room', function () {
        before(function (done) {
            Room.count(function (err, cnt) {
                count = cnt
                done()
            })
        })

        describe('Invalid parameters', function () {
            it('should respond with error', function (done) {
                agent
                    .post('/room/new')
                    .send({'name': ''})
                    .expect('Content-Type', /html/)
                    .expect(400)
                    .end(done)
            })

            it('should not save to the database', function (done) {
                Room.count(function (err, cnt) {
                    count.should.equal(cnt)
                    done()
                })
            })
        })

        describe('Valid parameters', function () {
            it('should send room created', function (done) {
                agent
                    .post('/room/new')
                    .send({'name': 'foo'}) //Doesnt work with field
                    .expect('Content-Type', /html/)
                    .expect(200)
                    .end(done)
            })

            it('should insert a record to the database', function (done) {
                Room.count(function (err, cnt) {
                    cnt.should.equal(count + 1)
                    done()
                })
            })

            it('should save the room to the database', function (done) {
                Room
                    .findOne({name: 'foo'})
                    .exec(function (err, room) {
                        should.not.exist(err);
                        room.should.be.an.instanceOf(Room);
                        room.name.should.equal('foo');
                        done()
                    })
            })
        })
    })
    
    //Switches
    describe('POST /room/:id/switch/new', function () {
        before(function (done) {
            Room
                .findOne({name: 'foo'})
                .exec(function (err, room) {
                    id = room.id;
                    done()
                })
        });

        it('should return room not found', function (done) {
            agent
                .post('/room/0/switch/new')
                .send({'type': 'foo'})
                .expect(500)
                .expect(/not found/)
                .end(done)
        })

        it('should send switch added', function (done) {
            agent
                .post('/room/' + id + '/switch/new')
                .send({'type': 'foo'})
                .expect(200)
                .end(done)
        })
    })
    
    describe('GET /room/:id/switch/list', function () {
        before('create new switches', function (done) {
            async.parallel([
                function (cb) {
                    //near ones
                    agent
                        .post('/room/' + id + '/switch/new')
                        .send({'type': 't1'})
                        .end(cb)
                },
                function (cb) {
                    //near ones
                    agent
                        .post('/room/' + id + '/switch/new')
                        .send({'type': 't2'})
                        .end(cb)
                }
            ], done);
        })

        it('should return 3 switches', function (done) {
            agent
                .get('/room/' + id + '/switch/list')
                .set('X-Requested-With', 'XMLHttpRequest') //No need maybe later...
                .expect(200)
                .expect('Content-Type', /json/)
                .expect(function (res) {
                    if (res.body.length != 3) throw new Error("Instead 3 switches getting:" + res.body.length);
                })
                .end(done)
        })
    })
    

    //Sensors
    describe('POST /room/:id/sensor/new', function () {
        it('should return room not found', function (done) {
            agent
                .post('/room/' + id + '/sensor/new')
                .send({'type': 'foo'})
                .expect(500)
                .expect(/not found/)
                .end(done)
        })

        it('should send sensor added', function (done) {
            agent
                .post('/room/' + id + '/sensor/new')
                .send({'type': 'foo'})
                .expect(200)
                .end(done)
        })
    })

    describe('GET /room/:id/sensor/list', function () {
        before('create new sensors', function (done) {
            async.parallel([
                function (cb) {
                    //near ones
                    agent
                        .post('/room/' + id + '/sensor/new')
                        .send({'type': 't1'})
                        .end(cb)
                },
                function (cb) {
                    //near ones
                    agent
                        .post('/room/' + id + '/sensor/new')
                        .send({'type': 't2'})
                        .end(cb)
                }
            ], done);
        })

        it('should return 3 sensors', function (done) {
            agent
                .get('/room/' + id + '/sensor/list')
                .set('X-Requested-With', 'XMLHttpRequest') //No need maybe later...
                .expect(200)
                .expect('Content-Type', /json/)
                .expect(function (res) {
                    if (res.body.length != 3) throw new Error("Instead 3 sensors getting:" + res.body.length);
                })
                .end(done)
        })
    })

});