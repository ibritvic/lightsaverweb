var server = require('../bin/server'),
    mongoose = require('mongoose');

var Customer = mongoose.model('Customer');
var Room = mongoose.model('Room');

/* Clear data
 Customer.collection.remove();
 Room.collection.remove();
 */

var customer1 = new Customer({ name: 'TestCustomer', country:'Hrvatska', email:'a@ab.com' });
customer1.save(function (err) {
    if (err)
        console.log('Customer save error: ' + err);
});


var room1 = new Room ({ name:"FirstRoom", customer: customer1});

room1.save(function (err) {
    if (err)
        console.log('Room save error');
});

var room2 = new Room ({ name:"SecondRoom", customer: customer1});

room2.save(function (err) {
    if (err)
        console.log('Room save error');
});

var date = new Date(), switchActions = [], sensorValues = [], boolValue = false;
for (var i=1;i<4;i++){
        date.setDate(date.getDate() + i);
    for(var j=1; j<10;j++){
        date.setHours(date.getHours()+j);
        boolValue = !boolValue; //toggle
        switchActions.push({state: boolValue, time:date.toJSON()});
        sensorValues.push({value: 50 + j, time:date});
    }
}

var sw1 = { name:"switch1", actions: switchActions},
    sw2 = { name:"switch2", actions: switchActions},
    sen1 = { name:"sensor1", values: sensorValues},
    sen2 = { name:"sensor2", values: sensorValues};

console.log(sw1);

room1.addSwitch(sw1, null);
room1.addSwitch(sw2, null);

room2.addSwitch(sw1, null);
room2.addSwitch(sw2, null);

room1.addSensor(sen1);
room1.addSensor(sen2);

room2.addSensor(sen1);
room2.addSensor(sen2);

