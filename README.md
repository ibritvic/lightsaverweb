# LightSaverWeb
LightSaverWeb is cloud solution for LightSaver project.
It consists of web and API part

## Tests
>Run unit tests with mocha
```sh
$ npm install -g mocha 
$ cd "project root"
$ mocha
```