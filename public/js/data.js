var data = function test(){
	var date = new Date(), switchActions = [], sensorValues = [], state = 0;
	for (var i=1;i<4;i++){
		date.setDate(date.getDate() + i);
	    for(var j=1; j<14;j++){
		date.setHours(date.getHours()+j);
		state = state?0:1; //toggle state
		switchActions.push({state: state, time:date});
		var lux = Math.floor((Math.random() * 1000) + 1); //Lux level
		sensorValues.push({L: lux, time:date});
		date.setSeconds(date.getSeconds()+j);
		sensorValues.push({O: !state, time:date});
	    }
	    sensorValues.push({T: Math.floor((Math.random() * 30) + 1), time:date});
	}

	var sw1 = { name:"switch1", actions: switchActions},
	    sw2 = { name:"switch2", actions: switchActions},
	    sen1 = { name:"sensor1", values: sensorValues},
	    sen2 = { name:"sensor2", values: sensorValues};

	var room1 = { name:'Room 1', switches:[sw1], sensors:[sen1]},
	    room2 = {name:'Room 2', switches:[sw1], sensors:[sen1]};
	
	return JSON.stringify([room1, room2]);
};

