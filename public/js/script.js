function startTime() {
    var today = new Date();
    var h = today.getHours();
    var m = today.getMinutes();
    var s = today.getSeconds();
    var d = today.getDate();
    var month = today.getMonth() + 1
    var y = today.getFullYear();

    m = checkTime(m);
    month = checkTime(month);
    s = checkTime(s);
    d = checkTime(d);
    document.getElementById('sTime').innerHTML =
    h + ":" + m + ":" + s + " &nbsp;" + d + ". " + month + ". " + y;
    var t = setTimeout(startTime, 500);
}
function checkTime(i) {
    if (i < 10) {i = "0" + i};  // add zero in front of numbers < 10
    return i;
}


let rawTempData = [21.5, 22, 22.2, 25, 26, 27, 24, 26, 25, 24, 22, 21.9, 21.7, 22.8, 23, 23.1, 23.1, 23, 23, 23.1, 23.2, 23.4, 23.3, 23.2];
let rawLightData = [0, 0, 0, 0, 26, 150, 210, 540, 570, 578, 580, 780, 770, 850, 910, 810, 640, 630, 620, 440, 423, 12, 0, 0] ;
let rawOccupancyData = [0,0,0,0,0,0,0,1,1,1,1,1,1,1,1,1,1,1,1,1,0,0,0,0];

let hour = (new Date()).getHours(); 
let dataLabels = [];
for (var i = hour; i < 24; i++) dataLabels.push(i);
for (i = 0; i < hour; i++) dataLabels.push(i);

var tempData = [], lightData = [], occupancyData = [];
for(i=0; i < 24; i++){
	tempData[i] = rawTempData[dataLabels[i]-1];
	occupancyData[i] = rawOccupancyData[dataLabels[i]-1];
	lightData[i] = rawLightData[dataLabels[i]-1];
}
let data = { temperature: tempData, light: lightData , occupancy : occupancyData};

//temeprature1 data begin 
var configTemperature = {
        type: 'line',
        data: {
            labels: dataLabels,
            datasets: [{
                label: "Temperature",
                display: false,
                data: [22.5, 22.4, 22.2, 22.1, 22, 23, 23.2, 23.4, 23.8, 24, 24.1, 24.5, 24.7, 25, 24.8, 24.8, 24.9, 24.7, 24.5, 24.1, 24.2, 24, 23.8, 23.6],
                }]
            },
            options: {
                responsive: true,
                title:{
                    display:true,
                    text:'Temperature'
                },
                tooltips: {
                    mode: 'label',
                },
                hover: {
                    mode: 'dataset'
                },
                scales: {
                    xAxes: [{
                        display: true,
                        scaleLabel: {
                            show: true,
                            labelString: 'Hours'
                        }
                    }],
                    yAxes: [{
                        display: true,
                        scaleLabel: {
                            show: true,
                            labelString: 'Temperature'
                        },
                        ticks: {
                            stepSize: 2,
                            fontSize: 8,
                            suggestedMin: 18,
                            suggestedMax: 30,
                        }
                    }]
                }
            }
        };

//temeprature1 data end 
//temeprature2 data begin 
var configTemperature2 = {
        type: 'line',
        data: {
            labels: dataLabels,
            datasets: [{
                label: "Temperature",
                display: false,
                data: [21.5, 22, 22.2, 25, 26, 27, 24, 26, 25, 24, 22, 21.9, 21.7, 22.8, 23, 23.1, 23.1, 23, 23, 23.1, 23.2, 23.4, 23.3, 23.2],
                }]
            },
            options: {
                responsive: true,
                title:{
                    display:true,
                    text:'Temperature'
                },
                tooltips: {
                    mode: 'label',
                },
                hover: {
                    mode: 'dataset'
                },
                scales: {
                    xAxes: [{
                        display: true,
                        scaleLabel: {
                            show: true,
                            labelString: 'Hours'
                        }
                    }],
                    yAxes: [{
                        display: true,
                        scaleLabel: {
                            show: true,
                            labelString: 'Temperature'
                        },
                        ticks: {
                            stepSize: 2,
                            fontSize: 8,
                            suggestedMin: 18,
                            suggestedMax: 30,
                        }
                    }]
                }
            }
        };

//temeprature2 data end 
//occuppancy data end 
var configoccuppancy = {
        type: 'line',
        data: {
            labels: dataLabels,
            datasets: [{
                label: "Occupancy",
                display: false,
                data: [0,0,0,0,0,0,0,1,1,1,1,1,0,1,1,1,1,1,1,1,0,0,0,0],
                }]
            },
            options: {
                responsive: true,
                title:{
                    display:true,
                    text:'Occupancy'
                },
                tooltips: {
                    mode: 'label',
                },
                hover: {
                    mode: 'dataset'
                },
                scales: {
                    xAxes: [{
                        display: true,
                        scaleLabel: {
                            show: true,
                            labelString: 'Hours'
                        }
                    }],
                    yAxes: [{
                        display: true,
                        scaleLabel: {
                            show: true,
                            labelString: 'Occupancy'
                        },
                        ticks: {
                            stepSize: 1,
                            fontSize: 8,
                            suggestedMin: 0,
                            suggestedMax: 1,
                        }
                    }]
                }
            }
        };
//occuppany data end 
//occuppancy2 data end 
var configoccuppancy2 = {
        type: 'line',
        data: {
            labels: dataLabels,
            datasets: [{
                label: "Occupancy",
                display: false,
                data: [0,0,0,0,0,0,0,1,1,1,1,1,1,0,1,1,1,1,1,1,0,0,0,0],
                }]
            },
            options: {
                responsive: true,
                title:{
                    display:true,
                    text:'Occupancy'
                },
                tooltips: {
                    mode: 'label',
                },
                hover: {
                    mode: 'dataset'
                },
                scales: {
                    xAxes: [{
                        display: true,
                        scaleLabel: {
                            show: true,
                            labelString: 'Hours'
                        }
                    }],
                    yAxes: [{
                        display: true,
                        scaleLabel: {
                            show: true,
                            labelString: 'Occupancy'
                        },
                        ticks: {
                            stepSize: 1,
                            fontSize: 8,
                            suggestedMin: 0,
                            suggestedMax: 1,
                        }
                    }]
                }
            }
        };
//occuppany2 data end 
// lux data begin
var configLux = {
        type: 'line',
        data: {
            labels: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24],
            datasets: [{
                label: "Illuminance",
                display: false,
                data: [0, 0, 0, 0, 26, 150, 210, 540, 570, 578, 580, 780, 770, 850, 910, 810, 640, 630, 620, 440, 423, 12, 0, 0],
                }]
            },
            options: {
                responsive: true,
                title:{
                    display:true,
                    text:'Illuminance'
                },
                tooltips: {
                    mode: 'label',
                },
                hover: {
                    mode: 'dataset'
                },
                scales: {
                    xAxes: [{
                        display: true,
                        scaleLabel: {
                            show: true,
                            labelString: 'Hours'
                        }
                    }],
                    yAxes: [{
                        display: true,
                        scaleLabel: {
                            show: true,
                            labelString: 'Illuminance'
                        },
                        ticks: {
                            fontSize: 8,
                            suggestedMin: 0,
                            suggestedMax: 1000,
                        }
                    }]
                }
            }
        };
// lux data end
// lux data begin
var configLux2 = {
        type: 'line',
        data: {
            labels: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24],
            datasets: [{
                label: "Illuminance",
                display: false,
                data: [0, 0, 0, 0, 50, 175, 270, 420, 600, 650, 740, 800, 850, 890, 950, 920, 740, 530, 420, 390, 370, 50, 0, 0],
                }]
            },
            options: {
                responsive: true,
                title:{
                    display:true,
                    text:'Illuminance'
                },
                tooltips: {
                    mode: 'label',
                },
                hover: {
                    mode: 'dataset'
                },
                scales: {
                    xAxes: [{
                        display: true,
                        scaleLabel: {
                            show: true,
                            labelString: 'Hours'
                        }
                    }],
                    yAxes: [{
                        display: true,
                        scaleLabel: {
                            show: true,
                            labelString: 'Illuminance'
                        },
                        ticks: {
                            fontSize: 8,
                            suggestedMin: 0,
                            suggestedMax: 1000,
                        }
                    }]
                }
            }
        };
// lux data end
//lights data begin
var configLights = {
        type: 'pie',
        data: {
            datasets: [{
                data: [
                    9, 15
                ],
                backgroundColor: [
                    "#46BFBD",
                    "#F7464A",
                ],
            }],
            labels: [
                "On",
                "Off",
            ]
        },
        options: {
            responsive: true,
            title:{
                display:true,
                text:'Light state (last 24h)'
            }
        }
    };
//lights data end

//lights data begin
var configLights2 = {
        type: 'pie',
        data: {
            datasets: [{
                data: [
                    10, 14
                ],
                backgroundColor: [
                    "#46BFBD",
                    "#F7464A",
                ],
            }],
            labels: [
                "On",
                "Off",
            ]
        },
        options: {
            responsive: true,
            title:{
                display:true,
                text:'Light state (last 24h)'
            }
        }
    };
//lights data end

//savings data begin
var configSavings = {
        type: 'bar',
        data: {
            datasets: [{
                data: [
                    470, 1000
                ],
                backgroundColor: [
                    "#46BFBD",
                    "#F7464A",
                ],
            }],
            labels: [
                "System on",
                "System off",
            ]
        },
        options: {
            responsive: true,
            title:{
                display:true,
                text:'Saved energy in Watts'
            }
            
        }
    };
//savings data end
window.onload = function () {
    startTime(); 
    var occupancyRoom1 = document.getElementById("occupancy1").getContext("2d");
    var occupancyRoom2 = document.getElementById("occupancy2").getContext("2d");
    var temperatureRoom1 = document.getElementById("temp").getContext("2d");
    var temperatureRoom2 = document.getElementById("temp2").getContext("2d");
    var luxRoom1 = document.getElementById("lux").getContext("2d");
    var luxRoom2 = document.getElementById("lux2").getContext("2d");
    var lightsRoom1 = document.getElementById("lights").getContext("2d");
    var lightsRoom2 = document.getElementById("lights2").getContext("2d");
    //var savingsRoom1 = document.getElementById("savings").getContext("2d");
    //var savingsRoom2 = document.getElementById("savings2").getContext("2d");
    window.myLine = new Chart(occupancy1, configoccuppancy);
    window.myLine = new Chart(occupancy2, configoccuppancy2);
    window.myLine = new Chart(temperatureRoom1, configTemperature);
    window.myLine = new Chart(temperatureRoom2, configTemperature2);
    window.myLine = new Chart(luxRoom1, configLux);
    window.myLine = new Chart(luxRoom2, configLux2);
    window.myPie = new Chart(lightsRoom1, configLights);
    window.myPie = new Chart(lightsRoom2, configLights2);
    //window.bar = new Chart(savingsRoom1, configSavings);
    //window.bar = new Chart(savingsRoom2, configSavings);
    
};
